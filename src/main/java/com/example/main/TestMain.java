package com.example.main;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

import com.example.model.User;

public class TestMain {
	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		
		Class.forName("com.mysql.jdbc.Driver");
		
		JdbcRowSet rowSet = RowSetProvider.newFactory().createJdbcRowSet();
		rowSet.setUrl("jdbc:mysql://127.0.0.1:3306/test");
		rowSet.setUsername("root");
		rowSet.setPassword("");
		rowSet.setCommand("SELECT * FROM user");
		rowSet.execute();
		while (rowSet.next()) {
			User user = new User();
			user.setId(rowSet.getInt("id"));
			user.setUsername(rowSet.getString("username"));
			user.setFullName(rowSet.getString("fullname"));
			user.setBirthday(rowSet.getDate("birthday"));
			user.setPassword(rowSet.getString("password"));
			user.setSexual(rowSet.getBoolean("sexual"));
			System.out.println(user);
		}

	}
}
