package com.example.c3p0;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.example.dao.UserDao;
import com.example.dao.impl.UserDaoImpl;
import com.example.model.User;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public class ConnectionPool {

	public static void main(String[] args) {
		ConnectionPool connectionPool = new ConnectionPool();
		for (int i = 0; i < 20; i++) {
			ThreadUse threadUse = new ThreadUse(connectionPool);
			threadUse.start();
		}
	}

	private ResourceBundle resourceBundle = ResourceBundle.getBundle("application");
	private ComboPooledDataSource comSource;

	public ConnectionPool() {
		try {
			initSource();
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}

	public void initSource() throws PropertyVetoException {
		comSource = new ComboPooledDataSource();
		comSource.setDriverClass(resourceBundle.getString("driver"));
		comSource.setJdbcUrl(resourceBundle.getString("url"));
		comSource.setUser(resourceBundle.getString("user"));
		comSource.setPassword(resourceBundle.getString("password"));
		comSource.setMinPoolSize(2);
		comSource.setInitialPoolSize(2);
		comSource.setMaxPoolSize(5);
	}

	public Connection getConnection() {
		try {
			logPoolStatus();
			return comSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public synchronized void logPoolStatus() throws SQLException {
		System.out.println("+ Num of Connections: " + comSource.getNumConnections());
		System.out.println("+ Num of Idle Connections: " + comSource.getNumIdleConnections());
		System.out.println("+ Num of Busy Connections: " + comSource.getNumBusyConnections());
	}

}

class ThreadUse extends Thread {

	private ConnectionPool connectionPool;

	public ThreadUse(ConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

	@Override
	public void run() {
		Connection connection = connectionPool.getConnection();

		UserDao userDao = new UserDaoImpl(connection);
		User user = userDao.findByUsername2("admin");
		System.out.println(user);

	}
}
