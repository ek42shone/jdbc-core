package com.example.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.dao.UserDao;
import com.example.model.User;
import com.example.utils.ConnectionUtil;

public class UserDaoImpl implements UserDao {

	public static void main(String[] args) {
		UserDao userDao = new UserDaoImpl();

		// PreparedStatement
		User user = userDao.findByUsername("taikhoanfake' OR 1=1 #");
		System.out.println(user);

		// Statement
		User user1 = userDao.findByUsername1("taikhoanfake' OR 1=1 #");
		System.out.println(user1);

		// CallableStatement
		User user2 = userDao.findById(1);
		System.out.println(user2);

// 		Insert
//		User userInsert = new User();
//		userInsert.setUsername("user1");
//		userInsert.setPassword("123456");
//		userInsert.setFullName("User New");
//		userInsert.setBirthday(new Date(System.currentTimeMillis()));
//		userInsert.setSexual(true);
//		Integer id = userDao.insert(userInsert);
//		System.out.println(id);

		List<User> users = new ArrayList<>();
		for (int i = 0; i < 200; i++) {
			User userTemp = new User();
			userTemp.setUsername("username" + i);
			userTemp.setPassword("password" + i);
			userTemp.setFullName("fullname" + i);
			userTemp.setBirthday(new Date(System.currentTimeMillis()));
			userTemp.setSexual(false);
			users.add(userTemp);
		}
		Integer count = userDao.insertMany(users);
		System.out.println(count);

	}

	private Connection conn;

	public UserDaoImpl() {
	}

	public UserDaoImpl(Connection connection) {
		this.conn = connection;
	}

	@Override
	public User findByUsername(String username) {
		String sql = "SELECT * FROM user WHERE username = ?";
		Connection connection = ConnectionUtil.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, username);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setUsername(resultSet.getString("username"));
				user.setFullName(resultSet.getString("fullname"));
				user.setBirthday(resultSet.getDate("birthday"));
				user.setPassword(resultSet.getString("password"));
				user.setSexual(resultSet.getBoolean("sexual"));
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public User findByUsername1(String username) {
		String sql = "SELECT * FROM user WHERE username = '" + username + "'";
		Connection connection = ConnectionUtil.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			if (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setUsername(resultSet.getString("username"));
				user.setFullName(resultSet.getString("fullname"));
				user.setBirthday(resultSet.getDate("birthday"));
				user.setPassword(resultSet.getString("password"));
				user.setSexual(resultSet.getBoolean("sexual"));
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (statement != null) {
					statement.close();
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Integer insert(User user) {
		String sql = "INSERT user(username,password,fullname,birthday,sexual) VALUES(?,?,?,?,?)";
		Connection connection = ConnectionUtil.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setString(3, user.getFullName());
			preparedStatement.setDate(4, user.getBirthday());
			preparedStatement.setBoolean(5, user.isSexual());

			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();

			connection.commit();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				if (resultSet != null) {
					resultSet.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public void update(User user) {

	}

	@Override
	public User findById(Integer id) {
		String sql = "CALL getUserById(?)";
		Connection connection = ConnectionUtil.getConnection();
		CallableStatement callableStatement = null;
		try {
			callableStatement = connection.prepareCall(sql);
			callableStatement.setInt(1, id);
			ResultSet resultSet = callableStatement.executeQuery();
			if (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setUsername(resultSet.getString("username"));
				user.setFullName(resultSet.getString("fullname"));
				user.setBirthday(resultSet.getDate("birthday"));
				user.setPassword(resultSet.getString("password"));
				user.setSexual(resultSet.getBoolean("sexual"));
				return user;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Integer insertMany(List<User> list) {
		int count = 1;
		String sql = "INSERT user(username,password,fullname,birthday,sexual) VALUES(?,?,?,?,?)";
		Connection connection = ConnectionUtil.getConnection();
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(sql);
			for (User user : list) {
				preparedStatement.setString(1, user.getUsername());
				preparedStatement.setString(2, user.getPassword());
				preparedStatement.setString(3, user.getFullName());
				preparedStatement.setDate(4, user.getBirthday());
				preparedStatement.setBoolean(5, user.isSexual());
				preparedStatement.addBatch();

				if (count % 20 == 0) {
					preparedStatement.executeBatch();
					connection.commit();
				}

				count++;
			}

		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (preparedStatement != null) {
					preparedStatement.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return count - 1;
	}

	@Override
	public User findByUsername2(String username) {
		String sql = "SELECT * FROM user WHERE username = ?";
		Connection connection = ConnectionUtil.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, username);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setUsername(resultSet.getString("username"));
				user.setFullName(resultSet.getString("fullname"));
				user.setBirthday(resultSet.getDate("birthday"));
				user.setPassword(resultSet.getString("password"));
				user.setSexual(resultSet.getBoolean("sexual"));
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
