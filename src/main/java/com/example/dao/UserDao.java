package com.example.dao;

import java.util.List;

import com.example.model.User;

public interface UserDao {

	User findByUsername(String username); // PreparedStatement

	User findByUsername1(String username); // Statement

	User findById(Integer id); // CallableStatement

	User findByUsername2(String username);

	Integer insert(User user);

	Integer insertMany(List<User> list);

	void update(User user);

}
