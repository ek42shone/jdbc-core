package com.example.model;

import java.sql.Date;

public class User {

	private Integer id;
	private String username;
	private String password;
	private String fullName;
	private Date birthday;
	private boolean sexual;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public boolean isSexual() {
		return sexual;
	}

	public void setSexual(boolean sexual) {
		this.sexual = sexual;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", fullName=" + fullName
				+ ", birthday=" + birthday + ", sexual=" + sexual + "]";
	}

}
