package com.example.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConnectionUtil {

	private static final ResourceBundle resourceBundle;

	static {
		resourceBundle = ResourceBundle.getBundle("application");
	}

	public static Connection getConnection() {
		try {
			Class.forName(resourceBundle.getString("driver"));
			return DriverManager.getConnection(resourceBundle.getString("url"), resourceBundle.getString("user"),
					resourceBundle.getString("password"));
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
