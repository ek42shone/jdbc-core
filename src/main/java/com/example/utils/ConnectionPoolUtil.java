package com.example.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.example.dao.UserDao;
import com.example.dao.impl.UserDaoImpl;
import com.example.model.User;

public class ConnectionPoolUtil {
	public static void main(String[] args) {
		ConnectionPool connectionPool = new ConnectionPool();

		for (int i = 0; i < 10; i++) {
			ThreadUse threadConnection = new ThreadUse(connectionPool);
			threadConnection.start();
		}
	}
}

class AdvConnection extends Thread {

	private ConnectionPool connectionPool;
	private Connection connection;
	private int timeLive;
	private String nameAdvCon;

	public AdvConnection() {
	}

	public AdvConnection(ConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

	public AdvConnection(Connection connection, int timeLive, String nameAdvCon, ConnectionPool connectionPool) {
		this.connection = connection;
		this.timeLive = timeLive;
		this.nameAdvCon = nameAdvCon;
		this.connectionPool = connectionPool;
		this.start();
	}

	@Override
	public void run() {
		while (this.timeLive > 0) {
			System.out.println(this.timeLive);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.timeLive--;
			if (timeLive == 0) {
				this.connectionPool.getCONNECTIONS_POOL().remove(this);
				this.connectionPool.wakeUp();
			}
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public int getTimeLive() {
		return timeLive;
	}

	public void setTimeLive(int timeLive) {
		this.timeLive = timeLive;
	}

	public String getNameAdvCon() {
		return nameAdvCon;
	}

	public void setNameAdvCon(String nameAdvCon) {
		this.nameAdvCon = nameAdvCon;
	}

}

class ConnectionPool {

	private LinkedList<AdvConnection> CONNECTIONS_POOL = new LinkedList<>();
	private static final int MAX_SIZE_POOL = 5;

	public synchronized AdvConnection getConnection() {
		while (CONNECTIONS_POOL.size() >= MAX_SIZE_POOL) {
			try {
				System.out.println("Wait ..." + CONNECTIONS_POOL.size());
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		AdvConnection connection = new AdvConnection(ConnectionUtil.getConnection(), 10, "NAME", this);
		CONNECTIONS_POOL.offer(connection);
		return connection;
	}

	public void timeout() {
		for (int i = 0; i < CONNECTIONS_POOL.size(); i++) {
			AdvConnection connection = CONNECTIONS_POOL.get(i);
			connection.start();
		}
	}

	public void resetTimeout(AdvConnection connection) {
		if (CONNECTIONS_POOL.contains(connection)) {
			connection.setTimeLive(10);
		}
	}

	public synchronized boolean isConnectionClose(AdvConnection connection) {
		boolean result = false;
		try {
			if (connection.getConnection().isClosed()) {
				CONNECTIONS_POOL.remove(connection);
				result = true;
			}
			System.out.println(result);
			notifyAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public synchronized void wakeUp() {
		notifyAll();
	}

	public LinkedList<AdvConnection> getCONNECTIONS_POOL() {
		return CONNECTIONS_POOL;
	}

	public void setCONNECTIONS_POOL(LinkedList<AdvConnection> cONNECTIONS_POOL) {
		CONNECTIONS_POOL = cONNECTIONS_POOL;
	}

}

class ThreadUse extends Thread {
	private ConnectionPool connectionPool;

	public ThreadUse(ConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

	@Override
	public void run() {
		AdvConnection connection = connectionPool.getConnection();

		// QUERY USE AdvConnection
		UserDao userDao = new UserDaoImpl(connection.getConnection());
		User user = userDao.findByUsername2("admin");
		System.out.println(user);

		connectionPool.isConnectionClose(connection);
		connectionPool.resetTimeout(connection);

	}

	public ConnectionPool getConnectionPool() {
		return connectionPool;
	}

	public void setConnectionPool(ConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

}
