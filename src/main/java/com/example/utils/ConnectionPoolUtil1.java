package com.example.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.example.dao.UserDao;
import com.example.dao.impl.UserDaoImpl;
import com.example.model.User;

public class ConnectionPoolUtil1 {

	public static void main(String[] args) {
		ConnectionPoolUtil1 connectionPoolUtil1 = new ConnectionPoolUtil1();
		for (int i = 0; i < 10; i++) {
			ThreadUse1 threadUse1 = new ThreadUse1(connectionPoolUtil1);
			threadUse1.start();
		}
	}

	private LinkedList<Connection> CONNECTIONS_POOL = new LinkedList<>();
	private static final int MAX_SIZE_POOL = 5;

	public ConnectionPoolUtil1() {
		initPool();
	}

	public synchronized void initPool() {
		while (CONNECTIONS_POOL.size() < MAX_SIZE_POOL) {
			CONNECTIONS_POOL.add(ConnectionUtil.getConnection());
		}
		notifyAll();
	}

	public synchronized Connection getConnection() {
		while (CONNECTIONS_POOL.size() == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return CONNECTIONS_POOL.poll();
	}

	public synchronized boolean isCloseConnection(Connection connection) {
		try {
			if (connection.isClosed()) {
				initPool();
			} else {
				boolean result = CONNECTIONS_POOL.offer(connection);
				notifyAll();
				return result;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}

class ThreadUse1 extends Thread {

	private ConnectionPoolUtil1 connectionPoolUtil1;

	public ThreadUse1(ConnectionPoolUtil1 connectionPoolUtil1) {
		this.connectionPoolUtil1 = connectionPoolUtil1;
	}

	@Override
	public void run() {
		Connection connection = this.connectionPoolUtil1.getConnection();

		UserDao userDao = new UserDaoImpl(connection);
		User user = userDao.findByUsername2("admin");
		System.out.println(user);

		connectionPoolUtil1.isCloseConnection(connection);
	}
}
