package com.example.dbcp;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.apache.commons.dbcp2.BasicDataSource;

import com.example.dao.UserDao;
import com.example.dao.impl.UserDaoImpl;
import com.example.model.User;

public class ConnectionPool {
	public static void main(String[] args) {
		ConnectionPool connectionPool = new ConnectionPool();
		for (int i = 0; i < 10; i++) {
			ThreadUse threadUse = new ThreadUse(connectionPool);
			threadUse.start();
		}
	}

	private BasicDataSource basicDataSource;
	private ResourceBundle resourceBundle = ResourceBundle.getBundle("application");

	public ConnectionPool() {
		init();
	}

	public void init() {
		basicDataSource = new BasicDataSource();
		basicDataSource.setDriverClassName(resourceBundle.getString("driver"));
		basicDataSource.setUrl(resourceBundle.getString("url"));
		basicDataSource.setUsername(resourceBundle.getString("user"));
		basicDataSource.setPassword(resourceBundle.getString("password"));
		basicDataSource.setMinIdle(2);
		basicDataSource.setInitialSize(2);
		basicDataSource.setMaxIdle(5);
		basicDataSource.setMaxOpenPreparedStatements(3000);
	}

	public Connection getConnection() {
		try {
			return basicDataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}

class ThreadUse extends Thread {

	private ConnectionPool connectionPool;

	public ThreadUse(ConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

	@Override
	public void run() {
		Connection connection = connectionPool.getConnection();

		UserDao userDao = new UserDaoImpl(connection);
		User user = userDao.findByUsername("admin");
		System.out.println(user);
	}
}
